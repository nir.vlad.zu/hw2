import 'dotenv/config'
import 'express-async-errors'
import express from 'express'
import { notesRouter } from './api/notes/notes.routes.js'
import { connectDB } from './shared/db/connect.js'
import { notFound } from './shared/middleware/not-found.js'
import { errorHandler } from './shared/middleware/errorHandler.js'
import { authRouter } from './api/auth/auth.routes.js'
import { usersRouter } from './api/user/user.routes.js'
import { authenticationMiddleware } from './api/auth/middleware/auth.middleware.js'
const app = express()
// middleware
app.use(express.json())

// routes
app.use('/api/auth', authRouter)
app.use('/api/notes', authenticationMiddleware, notesRouter)
app.use('/api/users', authenticationMiddleware, usersRouter)
app.use(notFound)
app.use(errorHandler)

const PORT = process.env.PORT || 3000

;(async () => {
  try {
    await connectDB(process.env.MONGO_URI)
    app.listen(PORT, console.log(`Server is running on http://localhost:${PORT}`))
  } catch (err) {
    console.error(err)
  }
})()

// start()
