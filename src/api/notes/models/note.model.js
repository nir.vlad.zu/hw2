import mongoose from 'mongoose'

const NoteSchema = new mongoose.Schema({
  text: {
    type: String,
    required: [true, 'text is required'],
    trim: true,
    minlength: [5, 'can not be less then 5 chars'],
  },
  completed: {
    type: Boolean,
    default: false,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  createdBy: {
    type: mongoose.Types.ObjectId,
    ref: 'User',
    required: [true, 'Please provide user'],
  },
})

export const noteModel = mongoose.model('Note', NoteSchema)
