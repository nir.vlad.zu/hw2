import express from 'express'

import {
  getAllNotes,
  createNote,
  getNote,
  updateNote,
  checkNote,
  deleteNote,
} from './controllers/notes.controller.js'
export const notesRouter = express.Router()

notesRouter.route('/').get(getAllNotes).post(createNote)
notesRouter.route('/:id').get(getNote).put(updateNote).patch(checkNote).delete(deleteNote)
