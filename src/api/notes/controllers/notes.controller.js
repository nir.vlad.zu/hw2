import { noteModel as Note } from '../models/note.model.js'
import { NotFoundError, BadRequestError } from '../../../shared/errors/index.js'
import { StatusCodes } from 'http-status-codes'

const getAllNotes = async (req, res) => {
  const notes = await Note.find({ userId: req.userID }).sort('createdAt')
  res.status(StatusCodes.OK).json({ notes, count: notes.length })
}

const createNote = async (req, res) => {
  req.body.createdBy = req.user.userId
  const note = await Note.create(req.body)
  res.status(StatusCodes.CREATED).json({ note })
}

const getNote = async (req, res) => {
  const {
    user: { userId },
    params: { id: noteId },
  } = req
  const note = await Note.findOne({ _id: noteId, createdBy: userId })
  if (!note) {
    throw new NotFoundError(`No note with id: ${noteId}`)
  }
  res.status(StatusCodes.OK).json({ note })
}

// PUT
const updateNote = async (req, res) => {
  const {
    user: { userId },
    body: { text, completed },
    params: { id: noteId },
  } = req

  if (text === '' || completed === undefined) {
    throw new BadRequestError('Please provide a text')
  }

  const note = await Note.findOneAndUpdate({ _id: noteId, createdBy: userId }, req.body, {
    new: true,
    runValidators: true,
  })

  if (!note) {
    throw new NotFoundError(`No note with id: ${noteId}`)
  }
  res.status(StatusCodes.OK).json({ note })
}

// PATCH
const checkNote = async (req, res) => {
  const {
    params: { id: noteId },
    user: { userId },
  } = req

  const noteToUpdate = await Note.findOne({ _id: noteId, createdBy: userId })
  if (!noteToUpdate) {
    throw new NotFoundError(`No note with id: ${noteId}`)
  }
  noteToUpdate.completed = !noteToUpdate.completed
  await Note.updateOne({ _id: noteId, createdBy: userId }, noteToUpdate)

  res.status(StatusCodes.OK).json({ noteToUpdate })
}

const deleteNote = async (req, res) => {
  const {
    user: { userId },
    params: { id: noteId },
  } = req

  const note = await Note.findOneAndDelete({ _id: noteId, createdBy: userId })

  if (!note) {
    throw new NotFoundError(`No note with id: ${noteId}`)
  }

  res.status(StatusCodes.OK).send()
}

export { getAllNotes, createNote, getNote, updateNote, checkNote, deleteNote }
