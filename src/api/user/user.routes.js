import express from 'express'
import { getUser, deleteUser, updateUser } from './controllers/user.controller.js'
export const usersRouter = express.Router()

usersRouter.route('/me').get(getUser).delete(deleteUser).patch(updateUser)
