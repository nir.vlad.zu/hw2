import bcrypt from 'bcryptjs'
import { StatusCodes } from 'http-status-codes'
import { BadRequestError } from '../../../shared/errors/badRequest.error.js'
import { NotFoundError } from '../../../shared/errors/notFound.error.js'
import { userModel as User } from '../models/user.model.js'

export const getUser = async (req, res, next) => {
  const user = await User.findOne({ _id: req.user.userId }).select('-password')
  res.status(StatusCodes.OK).json({ user })
}

export const deleteUser = async (req, res, next) => {
  const {
    user: { userId },
  } = req

  const user = await User.findOneAndDelete({ _id: userId })

  if (!user) {
    throw new NotFoundError(`No user with id: ${userId}`)
  }

  res.status(StatusCodes.OK).send()
}

export const updateUser = async (req, res, next) => {
  const {
    user: { userId },
    body: { username, password },
  } = req

  if (username === '' || password === '') {
    throw new BadRequestError('Please provide full credentials')
  }

  if (password) {
    try {
      req.body.password = await bcrypt.hash(req.body.password, 10)
    } catch (err) {
      throw new BadRequestError('Whoops')
    }
  }

  const user = await User.findOneAndUpdate({ _id: userId }, req.body, {
    new: true,
    runValidators: true,
  })

  if (!user) {
    throw new NotFoundError(`No user with id: ${userId}`)
  }

  res.status(StatusCodes.OK).json({ user })
}
