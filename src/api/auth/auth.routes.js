import express from 'express'
export const authRouter = express.Router()
import { login, register } from './controllers/auth.controller.js'

authRouter.route('/register').post(register)
authRouter.route('/login').post(login)
